var myTheme = {
	init : function(){
		var ie_v = $exe.isIE();
		if (ie_v && ie_v<8) return false;
		setTimeout(function(){
			$(window).resize(function() {
				myTheme.reset();
			});
		},1000);
		var l = $('<p id="nav-toggler"><a href="#" onclick="myTheme.toggleMenu(this);return false" class="hide-nav" id="toggle-nav" title="'+$exe_i18n.hide+'"><span>'+$exe_i18n.menu+'</span></a></p>');
		$("#siteNav").before(l);
		$("#topPagination .pagination").prepend('<a href="#" onclick="window.print();return false" title="'+$exe_i18n.print+'" class="print-page"><span>'+$exe_i18n.print+'</span></a> ');
		this.addNavArrows();
		this.bigInputs();		
		var url = window.location.href;
		url = url.split("?");
		if (url.length>1){
			if (url[1].indexOf("nav=false")!=-1) {
				myTheme.hideMenu();
				return false;
			}
		}
		myTheme.setNavHeight();
		// We execute this more than once because sometimes the height changes because of the videos, etc.
		setTimeout(function(){
			myTheme.setNavHeight();
		},1000);
		$(window).load(function(){
			myTheme.setNavHeight();
		});
	},
	isMobile : function(){
		try {
			document.createEvent("TouchEvent");
			return true; 
		} catch(e) {
			return false;
		}
	},	
	bigInputs : function(){
		if (this.isMobile()) {
			$(".MultiSelectIdevice,.MultichoiceIdevice,.QuizTestIdevice,.TrueFalseIdevice").each(function(){
				$('input:radio',this).screwDefaultButtons({
					image: 'url("_style_input_radio.png")',
					width: 30,
					height: 30
				});
				$('input:checkbox',this).screwDefaultButtons({
					image: 'url("_style_input_checkbox.png")',
					width: 30,
					height: 30
				});
				$(this).addClass("custom-inputs");
			});
		}		
	},
	addNavArrows : function(){
		$("#siteNav ul ul .daddy").each(
			function(){
				this.innerHTML+='<span> &#9658;</span>';
			}
		);
	},	
	hideMenu : function(){
		$("#siteNav").hide();
		$(document.body).addClass("no-nav");
		myTheme.params("add");
		$("#toggle-nav").attr("class","show-nav").attr("title",$exe_i18n.show);
	},
	setNavHeight : function(){
		var n = $("#siteNav");
		var c = $("#main-wrapper");
		var nH = n.height();
		var cH = c.height();
		var isMobile = $("#siteNav").css("float")=="none";
		if (cH<nH) {
			cH = nH;
			if (!isMobile) {
				var s = c.attr("style");
				if (s) c.css("min-height",cH+"px");
				else c.attr("style","height:auto!important;height:"+cH+"px;min-height:"+cH+"px");
			}
		}
		var h = (cH-nH+24)+"px";
		var m = 0;
		if (isMobile) {
			h = 0;
			m = "15px";
		} else if (n.css("display")=="table") {
			h = 0;
		}
		n.css({
			"padding-bottom":h,
			"margin-bottom":m
		});
	},
	toggleMenu : function(e){
		if (typeof(myTheme.isToggling)=='undefined') myTheme.isToggling = false;
		if (myTheme.isToggling) return false;
		
		var l = $("#toggle-nav");
		
		if (!e && $(window).width()<900 && l.css("display")!='none') return false; // No reset in mobile view
		if (!e) l.attr("class","show-nav").attr("title",$exe_i18n.show); // Reset
		
		myTheme.isToggling = true;
		
		if (l.attr("class")=='hide-nav') {       
			l.attr("class","show-nav").attr("title",$exe_i18n.show);
			$("#siteFooter").hide();
			$("#siteNav").slideUp(400,function(){
				$(document.body).addClass("no-nav");
				$("#siteFooter").show();
				myTheme.isToggling = false;
			}); 
			myTheme.params("add");
		} else {
			l.attr("class","hide-nav").attr("title",$exe_i18n.hide);
			$(document.body).removeClass("no-nav");
			$("#siteNav").slideDown(400,function(){
				myTheme.isToggling = false;
				myTheme.setNavHeight();
			});
			myTheme.params("delete");            
		}
		
	},
	param : function(e,act) {
		if (act=="add") {
			var ref = e.href;
			var con = "?";
			if (ref.indexOf(".html?")!=-1 || ref.indexOf(".htm?")!=-1) con = "&";
			var param = "nav=false";
			if (ref.indexOf(param)==-1) {
				ref += con+param;
				e.href = ref;                    
			}            
		} else {
			// This will remove all params
			var ref = e.href;
			ref = ref.split("?");
			e.href = ref[0];
		}
	},
	params : function(act){
		$("A",".pagination").each(function(){
			myTheme.param(this,act);
		});
	},
	reset : function() {
		myTheme.toggleMenu();        
		myTheme.setNavHeight();
	},
	getCustomIcons : function(){
		// Provisional solution so the user can use the iDevice Editor to choose an icon
		$(".iDevice_header").each(function(){
			var i = this.style.backgroundImage;
			if (i!="") $(".iDeviceTitle",this).css("background-image",i);
			this.style.backgroundImage = "none";
		});
	}
}
$(function(){
	if ($("body").hasClass("exe-web-site")) {
		myTheme.init();
	}
	myTheme.getCustomIcons();
});

let jsonActual;
const resultadosPorPagina = 10;
let paginaActual = 1;
let subContexto="";
let palabraBuscada = null;
let contextoActual = null;
let cardsOnPage=null;
let obtenerIdCardActual = null;
let areasEPT  = new Set();
let areasSTEM = new Set();

const nodeDecoration = document.getElementById("nodeDecoration");
const offcanvasElementList = document.querySelectorAll('.offcanvas')
const filtros=document.getElementById("filtros");
let offcanvas_header=document.getElementById("offcanvas_header");
const filtrosPadre=filtros.parentNode;


// Obtiene el elemento de entrada
const inputText = document.getElementById('input_text');

const recargarPagina=document.getElementById('reloadPage');
recargarPagina.addEventListener("click", ()=>{
	location.reload();
	recargarPagina.classList.add("active");
});

const formatOracion = (str) => {
	return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
}
  
const obtenerData = async () => {
    try {
        const response = await fetch('./fichero.json');
        const data = await response.json();
        
        jsonActual = objetoFiltrado(data); // Asignación directa
		
		jsonActual.ept.forEach(dato=>{
			dato.Area = formatOracion(dato.Area);
			dato.SubArea = formatOracion(dato.SubArea);
		})
		jsonActual.ept.forEach(ept => {
			areasEPT.add(ept["Area"]);  
		});
		jsonActual.simuladores.forEach(stem => {
			stem["Areas"].forEach(area => {
				areasSTEM.add(area);  // Añade cada área al Set, que automáticamente evita duplicados
			});
		});
    } catch (error) {
        console.error('Error fetching or parsing JSON:', error);
    }
};

const mixEducacionPorNiveles=(educacion)=> {
	// Filtrar cada nivel dentro del objeto educacion
	const cartasInicial = educacion.inicial;
	const cartasPrimaria = educacion.primaria;
	const cartasSecundaria = educacion.secundaria;

	// Devolver el objeto filtrado completo con todos los niveles
	return [
		...cartasInicial,
		...cartasPrimaria,
		...cartasSecundaria
	];
}

//MEZCLA LA INFORMACIÓN DE EDUCA, EPT Y STEM en un array
const mezclar_data = async (data)=>{

	const { educacion, ept, simuladores } = data;

	// Recopilar todas las cartas de los conjuntos de datos
    const cartasEducacion = mixEducacionPorNiveles(educacion);
    const cartasEPT = ept;
    const cartasSimuladores = simuladores;

    // Crear un arreglo de cartas combinadas
    const cartasCombinadas = [
        ...cartasEducacion,
        ...cartasEPT,
        ...cartasSimuladores
    ];
	return cartasCombinadas;
}

const objetoFiltrado = (objeto) => {
    // Función para verificar si un objeto tiene las propiedades 'id' y 'titulo'
    const tieneIdYTitulo = (obj) => obj.Titulo.trim() !== "" && obj.Resumen.trim() !== "";
    const tieneCursoEintro = (obj) => obj.Curso.trim() !== "" && obj["Referencia Pedagógica"].Introduccion.trim() !== '';
    const tieneSimulYdescp = (obj) => obj.Simulador.trim() !== "" && obj.Descripcion.trim() !== "";

	// Filtrar niveles de educación
    const educacionInicialFiltrada = objeto.educacion.inicial.filter(tieneIdYTitulo);
    const educacionPrimariaFiltrada = objeto.educacion.primaria.filter(tieneIdYTitulo);
    const educacionSecundariaFiltrada = objeto.educacion.secundaria.filter(tieneIdYTitulo);

    const eptFiltrada = objeto.ept.filter(tieneCursoEintro);
    const simuladoresFiltrada = objeto.simuladores.filter(tieneSimulYdescp);

    // Devolver el objeto filtrado completo
    return {
        educacion: {
            inicial: educacionInicialFiltrada,
            primaria: educacionPrimariaFiltrada,
            secundaria: educacionSecundariaFiltrada
        },
        ept: eptFiltrada,
        simuladores: simuladoresFiltrada
    };
};

const compareNames = (a, b) => {
    // Obtener los IDs de a y b y convertirlos a minúsculas para estandarización
    const idA = a.id.toLowerCase();
    const idB = b.id.toLowerCase();

    // Extraer el tipo de cada ID
    const tipoA = idA.split('_')[0]; // Obtener la primera parte del ID
    const tipoB = idB.split('_')[0]; // Obtener la primera parte del ID

    let nameA, nameB;
    // Determinar el atributo relevante para comparar, basándose en el tipo
    switch (tipoA) {
        case 'educa':
            nameA = a.Titulo ? a.Titulo.toUpperCase() : '';
            break;
        case 'ept':
            nameA = a.Curso ? a.Curso.toUpperCase() : '';
            break;
        case 'stem':
            nameA = a.Simulador ? a.Simulador.toUpperCase() : '';
            break;
        default:
            nameA = '';
    }

    switch (tipoB) {
        case 'educa':
            nameB = b.Titulo ? b.Titulo.toUpperCase() : '';
            break;
        case 'ept':
            nameB = b.Curso ? b.Curso.toUpperCase() : '';
            break;
        case 'stem':
            nameB = b.Simulador ? b.Simulador.toUpperCase() : '';
            break;
        default:
            nameB = '';
    }

    // Comparar los nombres o títulos obtenidos
    if (nameA < nameB) {
        return -1;
    }
    if (nameA > nameB) {
        return 1;
    }

    // Si los nombres son iguales, retornar 0
    return 0;
};

const resaltarPalabra = (texto, palabra) => {
	if (!palabra) return texto;
	// Escapa caracteres especiales para evitar problemas con expresiones regulares
	const terminoSeguro = palabra.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	const regex = new RegExp(`(${terminoSeguro})`, 'gi');
	return texto.replace(regex, '<span class="resaltado p-0" >$1</span>');
};

const crearCard = (id, titulo, descripcion, image_src, keywords, terminoBusqueda=null) => {

	let cardTableContainer=null;
	let cardTable = null;
	// Poner en mayuscula primera letra de las keywords
	keys=keywords.split(",");
	keys= keys.map(word=>{
		return formatOracion(word.trim());
	});
	keywords=keys.join(", ");

    // Crear el contenedor principal
    const cardContainer = document.createElement('div');
    cardContainer.id = id;
    cardContainer.className = 'cardContainer col-9 card mb-3 ps-0 pe-0 ';
	cardContainer.setAttribute('data-bs-toggle', 'offcanvas');
	cardContainer.setAttribute('data-bs-target', '#offcanvasRight');
	cardContainer.setAttribute('aria-controls', 'offcanvasRight');

	// Crear la estructura interna
    const rowDiv = document.createElement('div');

	if (id.includes('educa_inicial')) {
		rowDiv.className = 'row g-0 border-start border-3 rounded border-primary';
		rowDiv.addEventListener('mouseover', ()=>rowDiv.className= 'row g-0 border rounded border-3 border-primary');
		rowDiv.addEventListener('mouseout', ()=>rowDiv.className = 'row g-0 border-start rounded border-3 border-primary');
	} else if (id.includes('educa_primaria')) {
		rowDiv.className = 'row g-0 border-start rounded border-3 border-primary';
		rowDiv.addEventListener('mouseover', ()=>rowDiv.className= 'row g-0 border rounded border-3 border-primary');
		rowDiv.addEventListener('mouseout', ()=>rowDiv.className = 'row g-0 border-start rounded border-3 border-primary');
	} else if (id.includes('educa_secundaria')) {
		rowDiv.className = 'row g-0 border-start rounded border-3 border-primary';
		rowDiv.addEventListener('mouseover', ()=>rowDiv.className= 'row g-0 border rounded border-3 border-primary');
		rowDiv.addEventListener('mouseout', ()=>rowDiv.className = 'row g-0 border-start rounded border-3 border-primary');
	} else if (id.includes('ept')) {
		rowDiv.className = 'row g-0 border-start rounded border-3 border-danger';
		rowDiv.addEventListener('mouseover', ()=>rowDiv.className= 'row g-0 border rounded border-3 border-danger');
		rowDiv.addEventListener('mouseout', ()=>rowDiv.className = 'row g-0 border-start rounded border-3 border-danger');
	} else if (id.includes('stem')) {
		rowDiv.className = 'row g-0 border-start rounded border-start border-3 border-warning';
		rowDiv.addEventListener('mouseover', ()=>rowDiv.className= 'row g-0 border rounded border-3 border-warning');
		rowDiv.addEventListener('mouseout', ()=>rowDiv.className = 'row g-0 border-start rounded border-3 border-warning');
	}

	const colImgDiv = document.createElement('div');
    colImgDiv.className = 'col-xl-3 col-lg-4 col-md-5 col-xs-12 d-flex align-items-center justify-content-center';
	if( id.includes('ept')) {
		colImgDiv.classList.add('p-3');
	}
	
    const imgElement = document.createElement('img');
    imgElement.className = 'img-fluid rounded ';
	imgElement.style.border="solid 0 0 0 1px";
    imgElement.src = image_src;

    const colTextDiv = document.createElement('div');
    colTextDiv.className = 'col px-xs-4';

    const cardBodyDiv = document.createElement('div');
    cardBodyDiv.className = 'card-body';

    const cardTitle = document.createElement('h6');
    cardTitle.className = 'card-title';
	cardTitle.textContent = titulo;
	cardTitle.innerHTML = terminoBusqueda ? resaltarPalabra(cardTitle.textContent, terminoBusqueda) : cardTitle.textContent;

    const cardText1 = document.createElement('p');
    cardText1.style.textAlign = "justify";
    cardText1.className = 'card-text';
	cardText1.textContent = descripcion;
    cardText1.innerHTML = terminoBusqueda ? resaltarPalabra(cardText1.textContent, terminoBusqueda) : cardText1.textContent;
	
	if (obtenerContextoActual() === null && id.includes('educa')) {
		let itemActual ={};
		if(id.includes('educa_inicial')){
			itemActual= jsonActual.educacion.inicial.filter(educa => educa.id === id)[0];
		}
		else if(id.includes('educa_primaria')){
			itemActual= jsonActual.educacion.primaria.filter(educa => educa.id === id)[0];
		}
		else{
			itemActual= jsonActual.educacion.secundaria.filter(educa => educa.id === id)[0];
		}
		cardTableContainer = document.createElement('div');
		const divTable=document.createElement('div')
		cardTable = document.createElement('table');
		
		cardTableContainer.className ="d-flex justify-content-left";
		divTable.className = "col"
		cardTable.className = "table";
	
		// Utilizando for...in para iterar sobre las propiedades del objeto
		for (let prop in itemActual) {
			if (["Modalidad", "Nivel educativo/Ciclo", "Grado/Edad", "Área Curricular"].includes(prop) && itemActual[prop]) {
				const trElement = document.createElement("tr");
				const tdProp = document.createElement("td");
				const tdValue = document.createElement("td");
	
				tdProp.innerHTML = `${prop}`;
				tdProp.className = "text-black fw-bold";
				tdValue.textContent = itemActual[prop];
				tdValue.innerHTML = palabraBuscada ? resaltarPalabra(tdValue.textContent, palabraBuscada) : tdValue.textContent;
				tdValue.className = "text-black";
	
				trElement.appendChild(tdProp);
				trElement.appendChild(tdValue);
				cardTable.appendChild(trElement);
			}
		}
		divTable.appendChild(cardTable)
		cardTableContainer.appendChild(divTable);
	}
	

    // Combinar los elementos en la estructura correcta
    cardContainer.appendChild(rowDiv);
    
	rowDiv.appendChild(colImgDiv);
    colImgDiv.appendChild(imgElement);
    rowDiv.appendChild(colTextDiv);
    colTextDiv.appendChild(cardBodyDiv);
    cardBodyDiv.appendChild(cardTitle);
    cardBodyDiv.appendChild(cardText1);
	if(cardTableContainer!==null && obtenerContextoActual()===null){
		cardBodyDiv.appendChild(cardTableContainer);
	}

	if(keywords.trim()!== ""){
		const cardText2 = document.createElement('p');
		cardText2.className = 'card-text';
		cardText2.textContent = keywords;
		cardText2.innerHTML = terminoBusqueda ? ("<b>Palabras clave: </b>" +resaltarPalabra(cardText2.textContent, terminoBusqueda)) : ("<b>Palabras clave: </b>" +cardText2.textContent);
		cardBodyDiv.appendChild(cardText2);
	}
    

    // Agregar el contenedor principal al documento
    const cards = document.getElementById("cards");
    cards.appendChild(cardContainer);
}

const limpiar_cards=()=>{
	const cards = document.getElementById('cards');

    // Verificar si se encontró el elemento
    if (cards) {
        // Eliminar todo el contenido del elemento
        cards.innerHTML = '';
    } else {
        console.error('No se encontró ningún elemento con el ID "cards".');
    }
}

const data_en_card = async (data, terminoBusqueda=null) => {
	limpiar_cards();

    const cartasCombinadas= await mezclar_data(data);
	cartasCombinadasOrdenadas=cartasCombinadas.sort(compareNames);
	
    // Mientras haya cartas por mostrar
    cartasCombinadasOrdenadas.forEach(cartaSeleccionada=>{
        // Determinar el tipo de carta
        let tipoCarta;
        let image_src;
        if (data.educacion.inicial.includes(cartaSeleccionada) || data.educacion.primaria.includes(cartaSeleccionada) || data.educacion.secundaria.includes(cartaSeleccionada)) {
			tipoCarta = 'Educacion';
			image_src = cartaSeleccionada["thumbnail"]; 
		} 
        else if (data.ept.includes(cartaSeleccionada)) {
            tipoCarta = 'EPT';
            image_src = cartaSeleccionada["thumbnail"]
        }
        else tipoCarta = 'Simuladores';

        // Imprimir la carta
        if (tipoCarta.includes("Educacion")) {
            crearCard(cartaSeleccionada.id, cartaSeleccionada.Titulo, cartaSeleccionada.Resumen, image_src, cartaSeleccionada.Tema,terminoBusqueda);
        }
        else if (tipoCarta.includes("EPT")) {
            crearCard(cartaSeleccionada.id, cartaSeleccionada.Curso, cartaSeleccionada["Referencia Pedagógica"].Introduccion, image_src, (cartaSeleccionada.Area + ", " + cartaSeleccionada.SubArea),terminoBusqueda);
        }
        else {
            crearCard(cartaSeleccionada.id, cartaSeleccionada.Simulador, cartaSeleccionada.Descripcion, cartaSeleccionada.Img, cartaSeleccionada.Temas,terminoBusqueda);
        }
    });
}

// Función para crear un elemento <li> para el area de filtros
const crearElementoLi=(area, count)=>{
    // Crear un nuevo elemento <li>
    let nuevoElemento = document.createElement('li');
    // Establecer el ID, clase, y contenido del elemento <li>
    nuevoElemento.id = "filter_" + area;
    nuevoElemento.className = "list-group-item btn text-start";
    nuevoElemento.style.background = "#f1f1f1";
    nuevoElemento.textContent = area + " (" + count + ")"; // Agregar el nombre del área y el recuento de elementos
    
    return nuevoElemento;
}

const obtenerContextoActual = () => (contextoActual);

const actualizarJsonActual = (resultadosFiltrados, contextoActual) => {
	// Actualizar el objeto jsonActual según el contexto actual y los resultados filtrados
	let jsonActualizado = {};
	
	switch (contextoActual) {
		case 'educacion':
			jsonActualizado = {
				educacion: {
					inicial: [],
					primaria: [],
					secundaria: []
				},
				ept: [],
				simuladores: []
			};
			resultadosFiltrados.forEach(item => {
				let id = item.id.toLowerCase();
				if (id.includes('educa_inicial')) {
					jsonActualizado.educacion.inicial.push(item);
				} else if (id.includes('educa_primaria')) {
					jsonActualizado.educacion.primaria.push(item);
				} else if (id.includes('educa_secundaria')) {
					jsonActualizado.educacion.secundaria.push(item);
				}
			});
			break;

		case 'ept':
			jsonActualizado = {
				educacion: {
					inicial: [],
					primaria: [],
					secundaria: []
				},
				ept: resultadosFiltrados,
				simuladores: []
			};
			break;
		case 'simuladores':
			jsonActualizado = {
				educacion: {
					inicial: [],
					primaria: [],
					secundaria: []
				},
				ept: [],
				simuladores: resultadosFiltrados
			};
			break;
		default:
			jsonActualizado = {
				educacion: {
					inicial: [],
					primaria: [],
					secundaria: []
				},
				ept: [],
				simuladores: []
			};
			resultadosFiltrados.forEach(item => {
				let id = item.id.toLowerCase();
				if (id.includes('educa_inicial')) {
					jsonActualizado.educacion.inicial.push(item);
				} else if (id.includes('educa_primaria')) {
					jsonActualizado.educacion.primaria.push(item);
				} else if (id.includes('educa_secundaria')) {
					jsonActualizado.educacion.secundaria.push(item);
				} else if (id.includes('ept')) {
					jsonActualizado.ept.push(item);
				} else if (id.includes('stem')) {
					jsonActualizado.simuladores.push(item);
				}
			});
	}
	return jsonActualizado;
};

const actualizarEstadoBotones = () => {
    const maxPaginas = Math.ceil(tamanioJsonActual(jsonActual) / resultadosPorPagina);
    const startPage = Math.floor((paginaActual - 1) / 5) * 5 + 1;
    const endPage = Math.min(startPage + 4, maxPaginas);

    const btnAnterior = document.querySelector('.btn_anterior');
    const btnSiguiente = document.querySelector('.btn_siguiente');
	const rango_resultados=document.getElementById("rango_resultados");
	const total_cant_resultados=document.getElementById("total_cant_resultados");
	if(endPage==paginaActual){
		rango_resultados.textContent = `${10*(paginaActual-1)+1} - ${tamanioJsonActual(jsonActual)}`;
	}else{
		rango_resultados.textContent = `${10*(paginaActual-1)+1} - ${10*paginaActual}`;
	}
	
	total_cant_resultados.textContent=`${tamanioJsonActual(jsonActual)}`;
	if (maxPaginas === 0) {
        btnAnterior.style.display = 'none';
        btnSiguiente.style.display = 'none';
    } else {
		btnAnterior.style.display = 'block'; // Asumiendo que los botones son tipo 'block' normalmente
        btnSiguiente.style.display = 'block';
		
		// Actualizar el estado y el estilo de los botones Anterior y Siguiente
		if (paginaActual === 1) {
			btnAnterior.classList.add('disabled');
			btnAnterior.querySelector('svg').setAttribute('fill', '#63676b');
		} else {
			btnAnterior.classList.remove('disabled');
			btnAnterior.querySelector('svg').setAttribute('fill', 'var(--color-header-tablas)');
		}
	
		if (paginaActual === maxPaginas || maxPaginas===0) {
			btnSiguiente.classList.add('disabled');
			btnSiguiente.querySelector('svg').setAttribute('fill', '#63676b');
		} else {
			btnSiguiente.classList.remove('disabled');
			btnSiguiente.querySelector('svg').setAttribute('fill', 'var(--color-header-tablas)');
		}
    }
    // Limpiar y reconstruir solo los botones numéricos
    const paginacionUl = document.querySelector('.paginacion');
    const existingLis = Array.from(paginacionUl.querySelectorAll('li'));
    existingLis.forEach(li => {
        if (li.querySelector('.page-link') && !li.querySelector('svg')) {
            paginacionUl.removeChild(li);
        }
    });

    for (let i = startPage; i <= endPage; i++) {
        const li = document.createElement('li');
        li.className = 'page-item ' + (i === paginaActual ? 'active' : '');
        const a = document.createElement('a');
        a.href = '#';
        a.className = 'page-link ' + (i === paginaActual ? 'fw-bold' : '');
        a.textContent = i;
        a.style.background = i === paginaActual ? 'rgba(5, 146, 85, 0.118)' : '';
        a.style.color = i === paginaActual ? 'var(--color-header-tablas)' : '';
        a.addEventListener('click', (e) => {
            e.preventDefault();
            paginaActual = i;
            cambiarPagina(paginaActual, jsonActual, palabraBuscada);
            actualizarEstadoBotones();
        });

        li.appendChild(a);
        paginacionUl.insertBefore(li, paginacionUl.children[paginacionUl.children.length - 1]);
    }
};

const tamanioJsonActual=(data)=>{
	const contexto=obtenerContextoActual();
	switch (contexto) {
		case 'educacion':
			return data.educacion.inicial.length+data.educacion.primaria.length +data.educacion.secundaria.length;

		case 'ept':
			return data.ept.length;

		case 'simuladores':
			return data.simuladores.length;

		default:
			return (data.educacion.inicial.length+data.educacion.primaria.length +data.educacion.secundaria.length + data.ept.length+data.simuladores.length);
	}
}

const cambiarPagina = async (pagina, data, terminoBusqueda=null) => {
	
	let inicio = (tamanioJsonActual(data) >=1) ? ((pagina - 1) * resultadosPorPagina) : 1; //Si la página tiene más de 1 elemento se irá actualizando, sino, comenzará en 1
	let fin=1;
	if(tamanioJsonActual(data)===0){
		fin =1;
	}
	else if(tamanioJsonActual(data)>=10){
		fin =(inicio + resultadosPorPagina);
	}else{
		fin=tamanioJsonActual(data);
	}

	let data_mezclada= await mezclar_data(data);
	data_mezclada=data_mezclada.sort(compareNames);
	
	const datosPagina = data_mezclada.slice(inicio, fin);
	data=actualizarJsonActual(datosPagina);
	await data_en_card(data, terminoBusqueda);

	actualizarEstadoBotones(pagina, Math.ceil(tamanioJsonActual(jsonActual) / resultadosPorPagina));
	cardsOnPage = document.getElementsByClassName('cardContainer');
	initCardListeners(cardsOnPage);
};

// Función para dar funcionalidad a la paginación en un div al final de la tabla
const paginar = () => {
    const btn_anterior = document.querySelectorAll('.btn_anterior');
    btn_anterior.forEach(btn => btn.addEventListener('click', () => {
        if (paginaActual > 1) {
            paginaActual--;
            cambiarPagina(paginaActual, jsonActual, palabraBuscada);
            actualizarEstadoBotones();
        }
    }));

    const btn_siguiente = document.querySelectorAll('.btn_siguiente');
    btn_siguiente.forEach(btn => btn.addEventListener('click', () => {
        const maxPaginas = Math.ceil(tamanioJsonActual(jsonActual) / resultadosPorPagina);
        if (paginaActual < maxPaginas) {
            paginaActual++;
            cambiarPagina(paginaActual, jsonActual, palabraBuscada);
            actualizarEstadoBotones();
        }
    }));

    actualizarEstadoBotones();
};

const initCardListeners = (cards)=> {
	// const offcanvasRight = document.getElementById('offcanvasRight');
	const offcanvasRightTitle =document.getElementById('offcanvasRightTitle');
	const offcanvasRightBody = document.getElementById('offcanvasRightBody');
	
    Array.from(cards).forEach(card => {
        card.addEventListener('click', () => {
            let obtenerIdCardActual = card.id;
			// Limpiar clases de fondo existentes
			offcanvas_header.classList.remove('bg-primary', 'bg-danger', 'bg-warning');

            if (obtenerIdCardActual.includes('educa_inicial')) {
				offcanvas_header.classList.add('bg-primary');
                const elementoSeleccionado = jsonActual.educacion.inicial.filter(dato => dato.id === obtenerIdCardActual)[0];
                offcanvasRightTitle.textContent = elementoSeleccionado.Titulo;
				
				
                offcanvasRightBody.innerHTML = "";
                const divContainer = document.createElement("div");
                divContainer.className = "px-2";
				let tbElement=null;
				const estado = elementoSeleccionado["url_local"]!==""
                for (let prop in elementoSeleccionado) {
					if(elementoSeleccionado[prop] != ""){
						if (["url","url_local"].includes(prop) ) {
							if(prop==="url_local" && estado===true){
								const divElementURL = document.createElement("div");
								const spanProp = document.createElement("span");
								const spanPropContent = document.createElement("span");
								const aElement=document.createElement('a');
								const path=`${window.location.protocol}//${window.location.hostname}${elementoSeleccionado[prop]}`;

								aElement.textContent=path;
								aElement.href=path;
								aElement.target='_blank';
								// a.style="text-decoration:none";
								divElementURL.className ="text-break pb-1";

								spanProp.innerHTML = `<b>Url: </b>`;
								spanPropContent.appendChild(aElement);
								divElementURL.appendChild(spanProp);
								divElementURL.appendChild(spanPropContent);
								divContainer.appendChild(divElementURL);
							}
							else if(prop==="url" && estado===false){
								const divElementURL = document.createElement("div");
								const spanProp = document.createElement("span");
								const aElement= document.createElement("a")
								const spanPropContent = document.createElement("span");

								spanProp.innerHTML = `<b>Url: </b>`;
								aElement.href=elementoSeleccionado[prop];
								aElement.target='_blank';
								aElement.textContent = elementoSeleccionado[prop];
								divElementURL.className="pb-1"
								
								spanPropContent.appendChild(aElement);
								divElementURL.appendChild(spanProp);
								divElementURL.appendChild(spanPropContent);
								divContainer.appendChild(divElementURL);
							}
						}
						else if (["Autor", "Resumen","Tema"].includes(prop)){
							const divElement = document.createElement("div");
							const spanProp = document.createElement("span");
							const spanPropContent = ["Autor","Resumen", "Tema", "url_local"].includes(prop) 
													? document.createElement("div")
													:document.createElement("span");
	
							spanProp.innerHTML = `<b>${prop}: </b>`;
							
							spanPropContent.textContent = elementoSeleccionado[prop];
							spanPropContent.innerHTML = palabraBuscada ? resaltarPalabra(spanPropContent.textContent, palabraBuscada) : spanPropContent.textContent;
							spanPropContent.style.textAlign = "justify";

	
							divElement.appendChild(spanProp);
							divElement.appendChild(spanPropContent);
							divContainer.appendChild(divElement);
						}
						else if (["Año", "Fuente","Modalidad", "Nivel educativo/Ciclo", "Grado/Edad", "Área Curricular"].includes(prop) && elementoSeleccionado[prop]) {
							// Asegurarse de que la tabla se cree solo una vez y solo si al menos uno de los campos es no vacío
							if (tbElement===null) {
								tbElement = document.createElement("table");
								tbElement.className = "table mt-2"
							}
						
							const trElement = document.createElement("tr");
							const tdProp = document.createElement("td");
							const tdValue = document.createElement("td");
						
							tdProp.innerHTML = `${prop}`;
							tdProp.className = " text-black fw-bold";
							tdValue.textContent = elementoSeleccionado[prop];
							tdValue.innerHTML = palabraBuscada ? resaltarPalabra(tdValue.textContent, palabraBuscada) : tdValue.textContent;
							tdValue.className = " text-black";

							trElement.appendChild(tdProp);
							trElement.appendChild(tdValue);
							tbElement.appendChild(trElement);

							// Verificar si la tabla fue creada y tiene al menos una fila antes de agregarla al contenedor
							if (tbElement && tbElement.rows.length > 0) {
								divContainer.appendChild(tbElement);
							} else {
								// Manejar el caso en que no se añaden filas porque todos los campos estaban vacíos
								console.log("No se añadió ninguna fila a la tabla porque los campos estaban vacíos o no están presentes.");
							}
						}
						
						else if(["Etrategia", "Competencia", "Enfoques"].includes(prop)){
							const divElement = document.createElement("div");
							const spanProp = document.createElement("span");
							const spanPropContent = document.createElement("span");
	
							spanProp.innerHTML = `<b>${prop}: </b>`;
							spanPropContent.textContent = elementoSeleccionado[prop];
							spanPropContent.innerHTML = palabraBuscada ? resaltarPalabra(spanPropContent.textContent, palabraBuscada) : spanPropContent.textContent;
							spanPropContent.style.textAlign = "justify";

							divElement.appendChild(spanProp);
							divElement.appendChild(spanPropContent);
							divContainer.appendChild(divElement);
						}
					}
                }
                offcanvasRightBody.appendChild(divContainer);
            } else if (obtenerIdCardActual.includes('educa_primaria')) {
                offcanvas_header.classList.add('bg-primary');
                const elementoSeleccionado = jsonActual.educacion.primaria.filter(dato => dato.id === obtenerIdCardActual)[0];
                offcanvasRightTitle.textContent = elementoSeleccionado.Titulo;
				
                offcanvasRightBody.innerHTML = "";
                const divContainer = document.createElement("div");
                divContainer.className = "px-2";
				
				let tbElement=null;
				const estado = elementoSeleccionado["url_local"]!==""
                for (let prop in elementoSeleccionado) {
					if(elementoSeleccionado[prop]!== ""){
						if (["url","url_local"].includes(prop) ) {
							if(prop==="url_local" && estado===true){
								const divElementURL = document.createElement("div");
								const spanProp = document.createElement("span");
								const spanPropContent = document.createElement("span");
								const aElement=document.createElement('a');
								const path=`${window.location.protocol}//${window.location.hostname}${elementoSeleccionado[prop]}`;

								aElement.textContent=path;
								aElement.href=path;
								aElement.target='_blank';
								// a.style="text-decoration:none";
								divElementURL.className ="text-break pb-1";

								spanProp.innerHTML = `<b>Url: </b>`;
								spanPropContent.appendChild(aElement);
								divElementURL.appendChild(spanProp);
								divElementURL.appendChild(spanPropContent);
								divContainer.appendChild(divElementURL);
							}
							else if(prop==="url" && estado===false){
								const divElementURL = document.createElement("div");
								const spanProp = document.createElement("span");
								const aElement= document.createElement("a")
								const spanPropContent = document.createElement("span");

								spanProp.innerHTML = `<b>Url: </b>`;
								aElement.href=elementoSeleccionado[prop];
								aElement.target='_blank';
								aElement.textContent = elementoSeleccionado[prop];
								divElementURL.className="pb-1"
								
								spanPropContent.appendChild(aElement);
								divElementURL.appendChild(spanProp);
								divElementURL.appendChild(spanPropContent);
								divContainer.appendChild(divElementURL);
							}
						}
						else if (["Autor", "Resumen","Tema"].includes(prop)){
							const divElement = document.createElement("div");
							const spanProp = document.createElement("span");
							const spanPropContent = ["Autor","Resumen", "Tema", "url_local"].includes(prop) 
													? document.createElement("div")
													:document.createElement("span");
	
							spanProp.innerHTML = `<b>${prop}: </b>`;
							
							spanPropContent.textContent = elementoSeleccionado[prop];
							spanPropContent.innerHTML = palabraBuscada ? resaltarPalabra(spanPropContent.textContent, palabraBuscada) : spanPropContent.textContent;
							spanPropContent.style.textAlign = "justify";

	
							divElement.appendChild(spanProp);
							divElement.appendChild(spanPropContent);
							divContainer.appendChild(divElement);
						}
						else if (["Año", "Fuente","Modalidad", "Nivel educativo/Ciclo", "Grado/Edad", "Área Curricular"].includes(prop) && elementoSeleccionado[prop]) {
							// Asegurarse de que la tabla se cree solo una vez y solo si al menos uno de los campos es no vacío
							if (tbElement===null) {
								tbElement = document.createElement("table");
								tbElement.className = "table mt-2"
							}
						
							const trElement = document.createElement("tr");
							const tdProp = document.createElement("td");
							const tdValue = document.createElement("td");
						
							tdProp.innerHTML = `${prop}`;
							tdProp.className = " text-black fw-bold";
							tdValue.textContent = elementoSeleccionado[prop];
							tdValue.innerHTML = palabraBuscada ? resaltarPalabra(tdValue.textContent, palabraBuscada) : tdValue.textContent;
							tdValue.className = " text-black";

							trElement.appendChild(tdProp);
							trElement.appendChild(tdValue);
							tbElement.appendChild(trElement);

							// Verificar si la tabla fue creada y tiene al menos una fila antes de agregarla al contenedor
							if (tbElement && tbElement.rows.length > 0) {
								divContainer.appendChild(tbElement);
							} else {
								// Manejar el caso en que no se añaden filas porque todos los campos estaban vacíos
								console.log("No se añadió ninguna fila a la tabla porque los campos estaban vacíos o no están presentes.");
							}
						}
						
						else if(["Etrategia", "Competencia", "Enfoques"].includes(prop)){
							const divElement = document.createElement("div");
							const spanProp = document.createElement("span");
							const spanPropContent = document.createElement("span");
	
							spanProp.innerHTML = `<b>${prop}: </b>`;
							spanPropContent.textContent = elementoSeleccionado[prop];
							spanPropContent.innerHTML = palabraBuscada ? resaltarPalabra(spanPropContent.textContent, palabraBuscada) : spanPropContent.textContent;
							spanPropContent.style.textAlign = "justify";

							divElement.appendChild(spanProp);
							divElement.appendChild(spanPropContent);
							divContainer.appendChild(divElement);
						}
					}
                }
                offcanvasRightBody.appendChild(divContainer);
            } else if (obtenerIdCardActual.includes('educa_secundaria')) {
                offcanvas_header.classList.add('bg-primary');
                const elementoSeleccionado = jsonActual.educacion.secundaria.filter(dato => dato.id === obtenerIdCardActual)[0];
                offcanvasRightTitle.textContent = elementoSeleccionado.Titulo;
				
                offcanvasRightBody.innerHTML = "";
                const divContainer = document.createElement("div");
                divContainer.className = "px-2";
				
				let tbElement=null;
                const estado = elementoSeleccionado["url_local"]!=="";	
                for (let prop in elementoSeleccionado) {
					if(elementoSeleccionado[prop]!== ""){
						if (["url","url_local"].includes(prop) ) {
							if(prop==="url_local" && estado===true){
								const divElementURL = document.createElement("div");
								const spanProp = document.createElement("span");
								const spanPropContent = document.createElement("span");
								const aElement=document.createElement('a');
								const path=`${window.location.protocol}//${window.location.hostname}${elementoSeleccionado[prop]}`;

								aElement.textContent=path;
								aElement.href=path;
								aElement.target='_blank';
								// a.style="text-decoration:none";
								divElementURL.className ="text-break pb-1";

								spanProp.innerHTML = `<b>Url: </b>`;
								spanPropContent.appendChild(aElement);
								divElementURL.appendChild(spanProp);
								divElementURL.appendChild(spanPropContent);
								divContainer.appendChild(divElementURL);
							}
							else if(prop==="url" && estado===false){
								const divElementURL = document.createElement("div");
								const spanProp = document.createElement("span");
								const aElement= document.createElement("a")
								const spanPropContent = document.createElement("span");

								spanProp.innerHTML = `<b>Url: </b>`;
								aElement.href=elementoSeleccionado[prop];
								aElement.target='_blank';
								aElement.textContent = elementoSeleccionado[prop];
								divElementURL.className="pb-1"
								
								spanPropContent.appendChild(aElement);
								divElementURL.appendChild(spanProp);
								divElementURL.appendChild(spanPropContent);
								divContainer.appendChild(divElementURL);
							}
						}
						else if (["Autor", "Resumen","Tema"].includes(prop)){
							const divElement = document.createElement("div");
							const spanProp = document.createElement("span");
							const spanPropContent = ["Autor","Resumen", "Tema", "url_local"].includes(prop) 
													? document.createElement("div")
													:document.createElement("span");
	
							spanProp.innerHTML = `<b>${prop}: </b>`;
							
							spanPropContent.textContent = elementoSeleccionado[prop];
							spanPropContent.innerHTML = palabraBuscada ? resaltarPalabra(spanPropContent.textContent, palabraBuscada) : spanPropContent.textContent;
							spanPropContent.style.textAlign = "justify";

	
							divElement.appendChild(spanProp);
							divElement.appendChild(spanPropContent);
							divContainer.appendChild(divElement);
						}
						else if (["Año", "Fuente","Modalidad", "Nivel educativo/Ciclo", "Grado/Edad", "Área Curricular"].includes(prop) && elementoSeleccionado[prop]) {
							// Asegurarse de que la tabla se cree solo una vez y solo si al menos uno de los campos es no vacío
							if (tbElement===null) {
								tbElement = document.createElement("table");
								tbElement.className = "table mt-2"
							}
						
							const trElement = document.createElement("tr");
							const tdProp = document.createElement("td");
							const tdValue = document.createElement("td");
						
							tdProp.innerHTML = `${prop}`;
							tdProp.className = " text-black fw-bold";
							tdValue.textContent = elementoSeleccionado[prop];
							tdValue.innerHTML = palabraBuscada ? resaltarPalabra(tdValue.textContent, palabraBuscada) : tdValue.textContent;
							tdValue.className = " text-black";

							trElement.appendChild(tdProp);
							trElement.appendChild(tdValue);
							tbElement.appendChild(trElement);

							// Verificar si la tabla fue creada y tiene al menos una fila antes de agregarla al contenedor
							if (tbElement && tbElement.rows.length > 0) {
								divContainer.appendChild(tbElement);
							} else {
								// Manejar el caso en que no se añaden filas porque todos los campos estaban vacíos
								console.log("No se añadió ninguna fila a la tabla porque los campos estaban vacíos o no están presentes.");
							}
						}
						
						else if(["Etrategia", "Competencia", "Enfoques"].includes(prop)){
							const divElement = document.createElement("div");
							const spanProp = document.createElement("span");
							const spanPropContent = document.createElement("span");
	
							spanProp.innerHTML = `<b>${prop}: </b>`;
							spanPropContent.textContent = elementoSeleccionado[prop];
							spanPropContent.innerHTML = palabraBuscada ? resaltarPalabra(spanPropContent.textContent, palabraBuscada) : spanPropContent.textContent;
							spanPropContent.style.textAlign = "justify";

							divElement.appendChild(spanProp);
							divElement.appendChild(spanPropContent);
							divContainer.appendChild(divElement);
						}
					}
                }
                offcanvasRightBody.appendChild(divContainer);
            } else if (obtenerIdCardActual.includes('ept')) {
                offcanvas_header.classList.add('bg-danger');
                const elementoSeleccionado = jsonActual.ept.filter(dato => dato.id === obtenerIdCardActual)[0];
                offcanvasRightTitle.textContent = elementoSeleccionado.Curso;
                offcanvasRightBody.innerHTML = "";

				const divContainer = document.createElement("div");
                divContainer.className = "px-2";
				
                for (let prop in elementoSeleccionado) {
					if(elementoSeleccionado[prop] !==""){
						if (["Area", "SubArea"].includes(prop) ) {
							const divElement = document.createElement("div");
							const spanProp = document.createElement("span");
							const spanPropContent = (prop==="Referencia Temática") ? document.createElement("div"):document.createElement("span");
	
							divElement.className = "pb-1";
							spanProp.innerHTML = `<b>${prop}: </b>`;
							spanPropContent.textContent = elementoSeleccionado[prop];
							spanPropContent.innerHTML = palabraBuscada ? resaltarPalabra(spanPropContent.textContent, palabraBuscada) : spanPropContent.textContent;
	
							spanPropContent.style.textAlign = "justify";
							
							divElement.appendChild(spanProp);
							divElement.appendChild(spanPropContent);
							divContainer.appendChild(divElement);
						}
						else if(prop==='Referencia Pedagógica'){
							let TieneServer= false;
							(elementoSeleccionado["Referencia Pedagógica"]["ServerEscuela"].trim()!=="") ? TieneServer=true : TieneServer=false;
							for(sub_prop in elementoSeleccionado[prop]){
								if(elementoSeleccionado[prop][sub_prop] !=="")
								{
									if(sub_prop==='ServerEscuela' && TieneServer){
										const divElementURL = document.createElement("div");
										const spanProp = document.createElement("span");
										const spanPropContent = document.createElement("span");
										const aElement=document.createElement('a');
										const path=`${window.location.protocol}//${window.location.hostname}${elementoSeleccionado[prop][sub_prop]}`;
	
										aElement.textContent=path;
										aElement.href=path;
										aElement.target='_blank';
										// a.style="text-decoration:none";
										divElementURL.className ="text-break pb-1";
	
										spanProp.innerHTML = `<b>${sub_prop}: </b>`;
										spanPropContent.appendChild(aElement);
										divElementURL.appendChild(spanProp);
										divElementURL.appendChild(spanPropContent);
										divContainer.appendChild(divElementURL);
									}
									else if(sub_prop=== 'URL' && TieneServer===false){
	
										const divElementURL = document.createElement("div");
										const spanProp = document.createElement("span");
										const aElement= document.createElement("a")
										const spanPropContent = document.createElement("span");
	
										spanProp.innerHTML = `<b>${sub_prop}: </b>`;
										aElement.href=elementoSeleccionado[prop][sub_prop];
										aElement.target='_blank';
										aElement.textContent = elementoSeleccionado[prop][sub_prop];
										divElementURL.className="pb-1"
										
										spanPropContent.appendChild(aElement);
										divElementURL.appendChild(spanProp);
										divElementURL.appendChild(spanPropContent);
										divContainer.appendChild(divElementURL);
									}
									else if(sub_prop==='Introduccion' ){
	
										const divElement = document.createElement("div");
										const spanProp = document.createElement("span");
										const spanPropContent = document.createElement("div");
	
										spanProp.innerHTML = `<b>${sub_prop}: </b>`;
										spanPropContent.textContent = elementoSeleccionado[prop][sub_prop];
										spanPropContent.innerHTML = palabraBuscada ? resaltarPalabra(spanPropContent.textContent, palabraBuscada) : spanPropContent.textContent;
										spanPropContent.style.textAlign = "justify";
										divElement.className="pb-1"
	
										divElement.appendChild(spanProp);
										divElement.appendChild(spanPropContent);
										divContainer.appendChild(divElement);
									}
								}
							}
						}
						else if(prop==='Referencia Temática'){
							const divContainerRefTem= document.createElement("div");
							divContainerRefTem.innerHTML =`	<p>
																<b> ${prop}:</b>
															</p>`;
							const divAccordion = document.createElement('div');
							const texto = elementoSeleccionado[prop];
							const regex = /Nivel\s*\d+\s*Lección\s*\d+:.*?(?=(Nivel\s*\d+\s*Lección\s*\d+:|$))/gs;
							let coincidencias;
							const secciones = [];

							while ((coincidencias = regex.exec(texto)) !== null) {
								secciones.push(coincidencias[0]);
							}

							const listaNiveles = []; // Este array almacenará los niveles y lecciones
							
							for(let seccion of secciones){
								const regexNivel = /Nivel\s*\d+/;
								const regexLeccion = /Lección\s*\d+/;
								const regexContenido = /: (.*)|: (.*)/; // Para capturar el contenido después de "Lección x:"

								const nivelMatch = seccion.match(regexNivel)[0]; // Obtiene el match de "Nivel x"
								const leccionMatch = seccion.match(regexLeccion)[0]; // Obtiene el match de "Lección x"
								const contenidoMatch = seccion.match(regexContenido)[1]; // Obtiene el contenido después de "Lección x:"
								
								// Verifica si el nivel ya existe
								let nivelIndex = listaNiveles.findIndex(nivel => nivel[0] === nivelMatch);
								if (nivelIndex === -1) {
									// Si el nivel no existe, lo añade con un array vacío para las lecciones
									nivelIndex = listaNiveles.length;
									listaNiveles.push([nivelMatch, []]);
								}
							
								// Añade la lección y su contenido al nivel correspondiente
								listaNiveles[nivelIndex][1].push([leccionMatch, contenidoMatch]);
							}

							for (let nivel of listaNiveles) {
								const divAcc_item = document.createElement('div');
								const h5Acc_headerNivel = document.createElement('h5');
								const btnAcc = document.createElement('button');
								const divAccContainerBody = document.createElement('div');
								const divAccBody = document.createElement('div');
								const ulLeccion = document.createElement("ul");
								ulLeccion.className = 'list-group list-group-flush';
								
								btnAcc.textContent = `Nivel ${nivel[0].match(/\d+/g)}`;
							
								divAccordion.className = 'accordion accordion-flush';
								divAccordion.id = `accordionFlush-${elementoSeleccionado["id"]}`;
							
								divAcc_item.className = 'accordion-item';
							
								h5Acc_headerNivel.className = 'accordion-header';
								h5Acc_headerNivel.id = btnAcc.textContent.replace(/\s+/g, '_');
								
								btnAcc.className = 'accordion-button collapsed p-3 fw-semibold';
								listaNiveles.length===1 ? btnAcc.classList.add('border-bottom'):false;
								btnAcc.type = 'button';
								btnAcc.setAttribute('data-bs-toggle', 'collapse');
								btnAcc.setAttribute('data-bs-target', `#flush-collapse-${elementoSeleccionado["id"]}-${h5Acc_headerNivel.id}`);
								btnAcc.setAttribute('aria-expanded', 'false');
								btnAcc.setAttribute('aria-controls', `flush-collapse-${elementoSeleccionado["id"]}-${h5Acc_headerNivel.id}`);
								
								divAccContainerBody.id = `flush-collapse-${elementoSeleccionado["id"]}-${h5Acc_headerNivel.id}`;
								divAccContainerBody.className = 'accordion-collapse collapse';
								divAccContainerBody.setAttribute('data-bs-parent', `#${divAccordion.id}`);
							
								divAccBody.className = 'accordion-body p-0';
							
								for (let leccion of nivel[1]) {
									const liLeccion = document.createElement("li");
									const divLeccionContenido = document.createElement("div");
									const textoAjustado = leccion[0].replace(/(Lección)(\d+)/, "$1_$2");
							
									liLeccion.className = `list-group-item ${textoAjustado.replace(/\s+/g, '_')} fw-semibold`;
									liLeccion.textContent = `Lección ${leccion[0].match(/\d+/g)}`;
							
									divLeccionContenido.innerHTML = palabraBuscada ? resaltarPalabra(leccion[1]?.trim(), palabraBuscada) : leccion[1];
									divLeccionContenido.className ="ms-2 fw-normal"
									divLeccionContenido.style.textAlign = 'justify';
							
									liLeccion.appendChild(divLeccionContenido);
									ulLeccion.appendChild(liLeccion);
								}
								divAcc_item.appendChild(h5Acc_headerNivel);
								divAcc_item.appendChild(divAccContainerBody);
								h5Acc_headerNivel.appendChild(btnAcc);
								divAccContainerBody.appendChild(divAccBody);
								
								divAccBody.appendChild(ulLeccion);
								divAccordion.appendChild(divAcc_item);
							}
							divContainerRefTem.appendChild(divAccordion); // Asegúrate de tener esta referencia definida
							divContainer.appendChild(divContainerRefTem);
						}
					}
                }
                offcanvasRightBody.appendChild(divContainer);
            } else {
                offcanvas_header.classList.add('bg-warning');
                const elementoSeleccionado = jsonActual.simuladores.filter(dato => dato.id === obtenerIdCardActual)[0];
                offcanvasRightTitle.textContent = elementoSeleccionado.Simulador;
                offcanvasRightBody.innerHTML = "";
                const divContainer = document.createElement("div");
                divContainer.className = "px-2";

                for (let prop in elementoSeleccionado) {
                    if (prop !== "id" && prop !== "Simulador" && prop !== "Img" && prop !== "Areas" && prop!=='Referencia Pedagógica' && elementoSeleccionado[prop] !== "") {
                        if (prop==="ServerEscuela" || prop=="URL"){
							const divElementURL = document.createElement("div");
							const spanProp = document.createElement("span");
							const spanPropContent = document.createElement("div");
							const aElement=document.createElement('a');
							const path=()=>{
								if(prop==="ServerEscuela") { 
									return`${window.location.protocol}//${window.location.hostname}/${elementoSeleccionado[prop]}`
								}else{
									return `${elementoSeleccionado[prop]}`;
								}
							};
							
							divElementURL.className="text-break pb-1"
							spanPropContent.style.textAlign = "justify";

							aElement.textContent=path();
							aElement.href=path();
							aElement.target='_blank';
							// a.style="text-decoration:none";
							spanProp.innerHTML = `<b>${prop}: </b>`;

							spanPropContent.appendChild(aElement);
							divElementURL.appendChild(spanProp);
							divElementURL.appendChild(spanPropContent);
							divContainer.appendChild(divElementURL);
						}else{
							const divElement = document.createElement("div");
							const spanProp = document.createElement("span");
							const spanPropContent = document.createElement("div");

							spanPropContent.style.textAlign = "justify";
							divElement.className="pb-1"
							spanProp.innerHTML = `<b>${prop}: </b>`;
							spanProp.className = "accordion";

							if(prop==="Objetivos de aprendizaje sugeridos"){
								elementoSeleccionado[prop].forEach(linea=>{
									const pElement=document.createElement("p");
									pElement.textContent = linea;
									pElement.innerHTML = palabraBuscada ? resaltarPalabra(pElement.textContent, palabraBuscada) : pElement.textContent;

									(linea==="Competencias relevantes:")?pElement.className="fw-bold mb-1":pElement.className = "mb-1";
									spanPropContent.appendChild(pElement);
								});
							}
							else{
								spanPropContent.textContent = elementoSeleccionado[prop];
							}
							divElement.appendChild(spanProp);
							divElement.appendChild(spanPropContent);
							divContainer.appendChild(divElement);
						}
                    }
					
                }
                offcanvasRightBody.appendChild(divContainer);
            }
        });
    });
}

const main = async () => {
	
    await obtenerData();
    const jsonData = JSON.parse(JSON.stringify(jsonActual));
    await cambiarPagina(paginaActual, jsonActual);
	paginar();
	
	const boton_educacion=document.getElementById("boton_educacion");
	const boton_ept=document.getElementById("boton_ept");
	const boton_stem=document.getElementById("boton_stem");
	const head_filter=document.getElementById("head_filter");


	// Función principal para filtrar las categorías de "educacion"
	const buscarEnjsonData = () => {
		
		// Obtén el texto de búsqueda
		palabraBuscada = document.getElementById('input_text').value.toLowerCase();
		
		// Obtén el contexto actual (por ejemplo, educacion, ept, simuladores)
		const contextoActual = obtenerContextoActual(); // Debes implementar esta función

		// Filtrar cartas según el contexto actual
		let cartasFiltradas = [];
		switch (contextoActual) {
			case 'educacion':
				switch (subContexto) {
					case "inicial":
						cartasFiltradas = jsonData.educacion.inicial;
						break;
					case "primaria":
						cartasFiltradas = jsonData.educacion.primaria;
						break;
					case "secundaria":
						cartasFiltradas = jsonData.educacion.secundaria;
						break;
					default:
						cartasFiltradas = mixEducacionPorNiveles(jsonData.educacion);
						break;
				}
				break;
			case 'ept':
				if(subContexto!==""){		
					for(area of areasEPT){
						if(subContexto===area){
							cartasFiltradas = jsonData.ept.filter(ept=>ept["Area"]===area);
							break;
						}
					};
				}else{
					cartasFiltradas=jsonData.ept;
				}
				break;
			case 'simuladores':
				if(subContexto!==""){					
					for(area of areasSTEM){
						if(subContexto===area){
							cartasFiltradas = jsonData.simuladores.filter(stem => stem["Areas"].includes(area));
							break;
						}
					};
				}else{
					cartasFiltradas = jsonData.simuladores;
				}
				break;
			default:
				// Si no hay un contexto específico, realizar una búsqueda general
				const cartasEducacion = mixEducacionPorNiveles(jsonData.educacion);
				const cartasEPT = jsonData.ept;
				const cartasSimuladores = jsonData.simuladores;
				cartasFiltradas = [
					...cartasEducacion,
					...cartasEPT,
					...cartasSimuladores
				];
		}
		// Filtra cartasCombinadas para encontrar elementos que coincidan con el texto de búsqueda
		const resultadosFiltrados = cartasFiltradas.filter(item => {
			// Busca en todas las propiedades del objeto, excepto 'id'
			return Object.entries(item).some(([key, value]) => {
				// Excluye la clave 'id'
				if (!["id", "url","url_local","URL", "ServerEscuela"].includes(key)) {
					// Verificar si la propiedad contiene la palabra buscada
					return String(value).toLowerCase().includes(palabraBuscada.toLowerCase());
				}
				return false;
			});
		});
		
		// Actualizar el objeto jsonActual según los resultados filtrados
		jsonActual = actualizarJsonActual(resultadosFiltrados, contextoActual);
		palabraBuscada = palabraBuscada.trim();	
		paginaActual=1;
		
		// Verificar si hay resultados
		if (resultadosFiltrados.length === 0) {
			limpiar_cards();
			const cards = document.getElementById("cards");
			const resultadosDiv = document.createElement('div');
			const p = document.createElement('p');
			
			p.className ="text-center m-4 p-3 rounded fw-semibold bg-white"
			p.textContent =`No se encontraron resultados para "${palabraBuscada}"`
			resultadosDiv.className = "d-flex justify-content-center ";
			
			resultadosDiv.appendChild(p);
			cards.appendChild(resultadosDiv);
			
			actualizarEstadoBotones(paginaActual, 0);
		} else {
			cambiarPagina(paginaActual, jsonActual, palabraBuscada);
		}
		
	  };

	// Escucha el evento 'input' en el campo de entrada y llama a la función de búsqueda si el valor no está vacío
	inputText.addEventListener('input', () => {
		palabraBuscada = inputText.value.trim();
		if (palabraBuscada === null || palabraBuscada==="") {
			contextoActual=obtenerContextoActual();
			switch (contextoActual) {
				case "educacion":
					if(subContexto!=""){
						switch (subContexto) {
							case "inicial":
								jsonActual=actualizarJsonActual(jsonData.educacion.inicial);			
								break;
							case "primaria":
								jsonActual=actualizarJsonActual(jsonData.educacion.primaria);
								break;
							case "secundaria":
								jsonActual=actualizarJsonActual(jsonData.educacion.secundaria);
								break;
						}
					}else{
						jsonActual=actualizarJsonActual(mixEducacionPorNiveles(jsonData.educacion), contextoActual);
					}
					break;

				case "ept":
					if(subContexto!=""){
						for(area of areasEPT){
							if(subContexto===area){
								jsonActual=actualizarJsonActual(jsonData.ept.filter(ept=>ept["Area"]===area), contextoActual);
								break;
							}
						};
					}else{
						jsonActual=actualizarJsonActual(jsonData.ept, contextoActual);
					}
					break;

				case "simuladores":
					if(subContexto!=""){
						for(area of areasSTEM){
							if(subContexto===area){
								jsonActual = actualizarJsonActual(jsonData.simuladores.filter(stem => stem["Areas"].includes(area)), contextoActual);
								break;
							}
						};
					}else{
						jsonActual=actualizarJsonActual(jsonData.simuladores, contextoActual);
					}
					break;
					
				default:
					jsonActual=JSON.parse(JSON.stringify(jsonData));
					break;
				}
			paginaActual = 1;
			cambiarPagina(paginaActual, jsonActual);
		}else{
			buscarEnjsonData();
		}
		
	});	

	boton_educacion.addEventListener("click", () => {
		
		limpiar_cards();
		contextoActual="educacion";
		subContexto="";
		
		filtrosPadre.classList.remove("collapse");
		boton_educacion.classList.add("active");
		boton_ept.classList.remove("active");
		boton_stem.classList.remove("active");
		recargarPagina.classList.remove("active");

		head_filter.innerHTML = "<span>Filtrar por</span><div>PeruEduca EBR</div>";
		let data = JSON.parse(JSON.stringify(jsonData));
	
		// Ordenar el JSON por el atributo 'Titulo' para educación
		let data_sorted=mixEducacionPorNiveles(data.educacion).sort(compareNames);
		jsonActual=actualizarJsonActual(data_sorted);

		filtros.innerHTML="";
		
		let nuevoLi = crearElementoLi("Inicial", jsonActual.educacion.inicial.length);
		filtros.appendChild(nuevoLi);
		let nuevoLi2 = crearElementoLi("Primaria", jsonActual.educacion.primaria.length);
		filtros.appendChild(nuevoLi2);
		let nuevoLi3 = crearElementoLi("Secundaria", jsonActual.educacion.secundaria.length);
		filtros.appendChild(nuevoLi3);

		AreasEnMenuFiltros = filtros.getElementsByTagName("li");

		Array.from(AreasEnMenuFiltros).forEach(nivel => {
			nivel.addEventListener("click", () => {
				
				// Remover la clase 'active' de todos los elementos
				Array.from(AreasEnMenuFiltros).forEach(otherArea => {
					otherArea.classList.remove('active');
				});
		
				// Agregar la clase 'active' al elemento que fue clickeado
				nivel.classList.add('active');

				let data = JSON.parse(JSON.stringify(jsonData));
		
				// Ordenar el JSON por el atributo 'Curso' para EPT
				let data_sorted=mixEducacionPorNiveles(data.educacion).sort(compareNames);

				jsonActual=actualizarJsonActual(data_sorted);
				
				const nameNivel = nivel.textContent.split("(")[0].trim();
				switch (nameNivel) {
					case "Inicial":
						jsonActual=actualizarJsonActual(jsonActual.educacion.inicial);			
						break;
					case "Primaria":
						jsonActual=actualizarJsonActual(jsonActual.educacion.primaria);
						break;
					case "Secundaria":
						jsonActual=actualizarJsonActual(jsonActual.educacion.secundaria);
						break;
				}
				paginaActual=1;
				subContexto=nameNivel.toLowerCase();
				if(palabraBuscada!==null || palabraBuscada!=="") {
					buscarEnjsonData();
				}
				else{			
					cambiarPagina(paginaActual, jsonActual, palabraBuscada);
				}
			});
		});

		paginaActual=1;	
		if(palabraBuscada!==null || palabraBuscada!=="") {
			buscarEnjsonData();
		}
		else{	
			cambiarPagina(paginaActual, jsonActual, palabraBuscada);
		}
	});
	
	boton_ept.addEventListener("click", () => {
		limpiar_cards();
		contextoActual="ept";
		subContexto="";

		filtrosPadre.classList.remove("collapse");
		boton_educacion.classList.remove("active");
		boton_ept.classList.add("active");
		boton_stem.classList.remove("active")
		recargarPagina.classList.remove("active");

		head_filter.innerHTML = "<span>Filtrar por</span><div>Educación para el trabajo</div>";

		let data = JSON.parse(JSON.stringify(jsonData));
		
		// Ordenar el JSON por el atributo 'Curso' para EPT
		let data_sorted=data.ept.sort(compareNames);
		jsonActual=actualizarJsonActual(data_sorted);

		let countByArea = data_sorted.reduce((acc, obj) => {
			let area = obj.Area;			
			acc[area] = (acc[area] || 0) + 1;
			return acc;
		}, {});
		filtros.innerHTML="";
		
		// Iterar sobre cada área en countByArea y agregar un <li> para cada una
		for (let area in countByArea) {
			let nuevoLi = crearElementoLi(formatOracion(area), countByArea[area]);
			filtros.appendChild(nuevoLi);
		}

		AreasEnMenuFiltros = filtros.getElementsByTagName("li");
		
		Array.from(AreasEnMenuFiltros).forEach(area => {
			area.addEventListener("click", () => {
				
				// Remover la clase 'active' de todos los elementos
				 Array.from(AreasEnMenuFiltros).forEach(otherArea => {
					otherArea.classList.remove('active');
				});
		
				// Agregar la clase 'active' al elemento que fue clickeado
				area.classList.add('active');

				let data = JSON.parse(JSON.stringify(jsonData));
		
				// Ordenar el JSON por el atributo 'Curso' para EPT
				let data_sorted=data.ept.sort(compareNames);
				jsonActual=actualizarJsonActual(data_sorted);
				
				const nameArea = area.textContent.split("(")[0].trim();
				jsonActual=actualizarJsonActual(jsonActual.ept.filter(ept=>ept.Area===formatOracion(nameArea)));
				paginaActual=1;
				subContexto=nameArea;
				if(palabraBuscada!==null || palabraBuscada!=="") {
					buscarEnjsonData();
				}
				else{			
					cambiarPagina(paginaActual, jsonActual, palabraBuscada);
				}
			});
		});

		paginaActual = 1;
		
		if(palabraBuscada!==null || palabraBuscada!=="") {
			buscarEnjsonData();
		}
		else{			
			cambiarPagina(paginaActual, jsonActual, palabraBuscada);
		}
	});
	
	boton_stem.addEventListener("click", () => {
		limpiar_cards();
		contextoActual="simuladores";
		subContexto="";

		filtrosPadre.classList.remove("collapse");
		boton_educacion.classList.remove("active");
		boton_ept.classList.remove("active");
		boton_stem.classList.add("active")
		recargarPagina.classList.remove("active");
		
		head_filter.innerHTML = "<span>Filtrar por</span><div>Simuladores STEM</div>";
		let data = JSON.parse(JSON.stringify(jsonData));
	
		// Ordenar el JSON por el atributo 'Simulador' para STEM
		let data_sorted= data.simuladores.sort(compareNames);
		jsonActual=actualizarJsonActual(data_sorted);

		let countByArea = data_sorted.reduce((acc, obj) => {
			if (obj.Areas) {
				obj.Areas.forEach(area => {
					acc[area] = (acc[area] || 0) + 1;
				});
			}
			return acc;
		}, {});
		
		filtros.innerHTML="";
		
		// Iterar sobre cada área en countByArea y agregar un <li> para cada una
		for (let area in countByArea) {
			let nuevoLi = crearElementoLi(area, countByArea[area]);
			filtros.appendChild(nuevoLi);
		}
		
		AreasEnMenuFiltros = filtros.getElementsByTagName("li");
		
		Array.from(AreasEnMenuFiltros).forEach(area => {
			area.addEventListener("click", () => {

				 // Remover la clase 'active' de todos los elementos
				 Array.from(AreasEnMenuFiltros).forEach(otherArea => {
					otherArea.classList.remove('active');
				});
		
				// Agregar la clase 'active' al elemento que fue clickeado
				area.classList.add('active');

				let data = JSON.parse(JSON.stringify(jsonData));
		
				// Ordenar el JSON por el atributo 'Simulador' para simulador
				let data_sorted=data.simuladores.sort(compareNames);
				jsonActual=actualizarJsonActual(data_sorted);

				const nameArea = area.textContent.split("(")[0].trim();
				jsonActual = actualizarJsonActual(jsonActual.simuladores.filter(stem => stem.Areas.includes(nameArea)));
				paginaActual=1;
				subContexto=nameArea;
				
				if(palabraBuscada!==null || palabraBuscada!=="") {
					buscarEnjsonData();
				}
				else{			
					cambiarPagina(paginaActual, jsonActual, palabraBuscada);
				}
			});
		});

		paginaActual = 1;
		
		if(palabraBuscada!==null || palabraBuscada!=="") {
			buscarEnjsonData();
		}
		else{			
			cambiarPagina(paginaActual, jsonActual, palabraBuscada);
		}
	});	
};

main();


const mainW = document.getElementById("main");
document.addEventListener("DOMContentLoaded", () => {
    const goTop = document.querySelector("#goTop");
    const barra_indicador = document.querySelector(".indicador_scroll");

    const obtener_pixeles_inicio = () => mainW.scrollTop; // Asegúrate de obtener el scrollTop de mainW

    const irArriba = () => {
        const px_inicio = obtener_pixeles_inicio();
        if (px_inicio > 0) {
            requestAnimationFrame(irArriba); // Modifica esto para llamar a irArriba, no a goTop.
            mainW.scrollTo(0, px_inicio - (px_inicio / 20));
        }
    };

    const indicarScroll = () => {
        if (obtener_pixeles_inicio() > 50) {
            goTop.className = "mostrar";
        } else {
            goTop.className = "ocultar";
        }
        let alto = mainW.scrollHeight - mainW.clientHeight;
        let avance_scroll = (obtener_pixeles_inicio() / alto) * 100;
        barra_indicador.style.width = `${avance_scroll}%`;
    };

    goTop.addEventListener("click", () => {
        irArriba();
    });

    mainW.addEventListener("scroll", () => {
        indicarScroll(); // Debes invocar la función, no solo referenciarla
    });
});

const mainTopCalc = () => {
    mainW.style.top = 0;
};
document.addEventListener("DOMContentLoaded", mainTopCalc);


const handleLicense =() => {
	const packageLicense = document.getElementById("packageLicense");
  
	const setMarginBottom = () => {
	  const isPortrait = window.matchMedia("(orientation: portrait)").matches;
	  if (isPortrait) {
		//Si es vertical marginbottom es 8.5em
		packageLicense.style.marginBottom = '12rem';
	  } else {
		//Si es horizontal marginbottom es 10.5em
		packageLicense.style.marginBottom = '10.5em';
	  }
	};
  
	setMarginBottom(); 
  
	window.addEventListener('resize', setMarginBottom);
	window.addEventListener('orientationchange', setMarginBottom);
  }
  
handleLicense(); // Llamada inicial para establecer el margen de acuerdo a la orientación inicial


/*!
 * ScrewDefaultButtons v2.0.6
 * http://screwdefaultbuttons.com/
 *
 * Licensed under the MIT license.
 * Copyright 2013 Matt Solano http://mattsolano.com
 *
 * Date: Mon February 25 2013
 */(function(e,t,n,r){var i={init:function(t){var n=e.extend({image:null,width:50,height:50,disabled:!1},t);return this.each(function(){var t=e(this),r=n.image,i=t.data("sdb-image");i&&(r=i);r||e.error("There is no image assigned for ScrewDefaultButtons");t.wrap("<div >").css({display:"none"});var s=t.attr("class"),o=t.attr("onclick"),u=t.parent("div");u.addClass(s);u.attr("onclick",o);u.css({"background-image":r,width:n.width,height:n.height,cursor:"pointer"});var a=0,f=-n.height;if(t.is(":disabled")){a=-(n.height*2);f=-(n.height*3)}t.on("disableBtn",function(){t.attr("disabled","disabled");a=-(n.height*2);f=-(n.height*3);t.trigger("resetBackground")});t.on("enableBtn",function(){t.removeAttr("disabled");a=0;f=-n.height;t.trigger("resetBackground")});t.on("resetBackground",function(){t.is(":checked")?u.css({backgroundPosition:"0 "+f+"px"}):u.css({backgroundPosition:"0 "+a+"px"})});t.trigger("resetBackground");if(t.is(":checkbox")){u.on("click",function(){t.is(":disabled")||t.change()});u.addClass("styledCheckbox");t.on("change",function(){if(t.prop("checked")){t.prop("checked",!1);u.css({backgroundPosition:"0 "+a+"px"})}else{t.prop("checked",!0);u.css({backgroundPosition:"0 "+f+"px"})}})}else if(t.is(":radio")){u.addClass("styledRadio");var l=t.attr("name");u.on("click",function(){!t.prop("checked")&&!t.is(":disabled")&&t.change()});t.on("change",function(){if(t.prop("checked")){t.prop("checked",!1);u.css({backgroundPosition:"0 "+a+"px"})}else{t.prop("checked",!0);u.css({backgroundPosition:"0 "+f+"px"});var n=e('input[name="'+l+'"]').not(t);n.trigger("radioSwitch")}});t.on("radioSwitch",function(){u.css({backgroundPosition:"0 "+a+"px"})});var c=e(this).attr("id"),h=e('label[for="'+c+'"]');h.on("click",function(){u.trigger("click")})}if(!e.support.leadingWhitespace){var c=e(this).attr("id"),h=e('label[for="'+c+'"]');h.on("click",function(){u.trigger("click")})}})},check:function(){return this.each(function(){var t=e(this);i.isChecked(t)||t.change()})},uncheck:function(){return this.each(function(){var t=e(this);i.isChecked(t)&&t.change()})},toggle:function(){return this.each(function(){var t=e(this);t.change()})},disable:function(){return this.each(function(){var t=e(this);t.trigger("disableBtn")})},enable:function(){return this.each(function(){var t=e(this);t.trigger("enableBtn")})},isChecked:function(e){return e.prop("checked")?!0:!1}};e.fn.screwDefaultButtons=function(t,n){if(i[t])return i[t].apply(this,Array.prototype.slice.call(arguments,1));if(typeof t=="object"||!t)return i.init.apply(this,arguments);e.error("Method "+t+" does not exist on jQuery.screwDefaultButtons")};return this})(jQuery);

 
import os
import shutil
import zipfile



def copyfile(src, dst):
    shutil.copyfile(src, dst)
    
def copytree(src, dst):
    shutil.copytree(src, dst)

def clone_folders(src_folder, dest_base_folder, names_list):
    for name in names_list:
        new_folder_path = os.path.join(dest_base_folder, name).replace('.pdf', '')

        if not os.path.exists(new_folder_path):
            print(f"Clonando carpeta para {name}...")
            try:
                copytree(src_folder, new_folder_path)
            except PermissionError as e:
                print(f"PermissionError: No se pudo clonar la carpeta para {name} debido a: {e}")
            except Exception as e:
                print(f"Unexpected error al clonar la carpeta para {name}: {e}")

def process_folders(dest_base_folder, names_list, pdf_src_path):
    for name in names_list:
        new_folder_path = os.path.join(dest_base_folder, name).replace('.pdf', '')

        if os.path.exists(new_folder_path):
            print(f"procesando {name}...")
            try:
                dest_pdf_folder = os.path.join(new_folder_path, "pdf", "web", name)
                pdf_path = os.path.join(".", pdf_src_path, name)
                
                print("________________________________________________________________")
                print("pdf_path: ", pdf_path)
                print("________________________________________________________________")
                print("dest_pdf_folder: ", dest_pdf_folder)
                print("________________________________________________________________")

                if not os.path.exists(pdf_path):
                    print(f"No se encontró el archivo PDF: {pdf_path}")
                    continue  # Saltar al siguiente archivo si el PDF no se encuentra

                copyfile(pdf_path, dest_pdf_folder)
                modify_index_file(new_folder_path, name)
                create_zip(new_folder_path, dest_base_folder, name)
                
            except PermissionError as e:
                print(f"PermissionError: No se pudo procesar {name} debido a: {e}")
            except FileNotFoundError as e:
                print(f"FileNotFoundError: No se pudo encontrar el archivo {pdf_path} debido a: {e}")
            except Exception as e:
                print(f"Unexpected error al procesar {name}: {e}")

def modify_index_file(new_folder_path, name):
    index_file_path = os.path.join(new_folder_path, 'index.html')
    with open(index_file_path, 'r', encoding='utf-8') as file:
        file_content = file.read()
    
    # Cambiar la línea correspondiente
    new_line = f'<iframe style="width: 100%;" src="./pdf/web/viewer.html?file={name}"></iframe>'
    file_content = file_content.replace(
        '<iframe style="width: 100%;" src="./pdf/web/viewer.html?file=Agamenon-Esquilo.pdf"></iframe>',
        new_line
    )

    # Escribir los cambios en el archivo
    with open(index_file_path, 'w', encoding='utf-8') as file:
        file.write(file_content)

def create_zip(new_folder_path, dest_base_folder, name):
    zip_file_path = os.path.join(dest_base_folder, "zips", f"{name}.zip")
    with zipfile.ZipFile(zip_file_path, 'w') as zipf:
        for root, dirs, files in os.walk(new_folder_path):
            for file in files:
                file_path = os.path.join(root, file)
                arcname = os.path.relpath(file_path, new_folder_path)
                zipf.write(file_path, arcname)

def list_files_in_folder(folder_path):
    try:
        file_names = [f for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path, f))]
        return file_names
    except Exception as e:
        print(f"Error al listar archivos en la carpeta: {e}")
        return []

def main():
    folder_path = 'C:/Users/adminabz/Documents/23 Elejandria-Biblioteca/epub-pdf/pdfs'
    file_names = list_files_in_folder(folder_path)

    src_folder = "C:/Users/adminabz/Documents/Biblioteca/Agamenon-Esquilo"
    dest_base_folder = "carpeta_prueba"
    names_list = file_names[slice(50, 52)]
    print(names_list)
    pdf_src_path = "epub-pdf/pdfs"
    
    # Fase 1: Clonación de carpetas
    clone_folders(src_folder, dest_base_folder, names_list)
    
    # Fase 2: Procesamiento de carpetas
    process_folders(dest_base_folder, names_list, pdf_src_path)

# Ejecutar el script principal
main()

#20, 51
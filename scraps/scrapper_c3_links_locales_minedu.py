import requests
import json

def concatResponse(response): #concatena las respuestas que son listas
    return ', '.join(response)

def concatResponse2(response):#concatena las respuestas que contienen una propiedad description en cada elemento de la lista
    l=[]
    for e in response:
        l.append(e.get("description"))
    return ', '.join(l)

def checkResponse(response): #verifica que la respuesta no esté vacía
    if response in ["--Seleccione--", "No aplica"]:
        return ""
    else:
        return response

def checkItem(item): #verifica que un item exista
    if item is not None:
        if isinstance(item, list) and len(item)==0:
            return False
        else:
            return True
    else:
        return False

def nombre_sin_extension(item):
    if checkItem(item.get("url")):
        nombre = item.get("url").split("/")[4]
        if nombre.endswith(".html"):
            return nombre.split(".html")[0]
        elif nombre.endswith(".png"):
            return nombre.split(".png")[0]
        else:
            return nombre
    else:
        return ""
# Configura la URL base, el endpoint y los encabezados con el token Bearer
url_base = "https://educacion.colaboraccion.pe/api"

bearer_token='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InByb2Zlc29yMDEiLCJzdWIiOiJDTj1wcm9mZXNvcjAxLE9VPUMzVGVhY2hlcixPVT1QZW9wbGUsREM9YzNlZHUsREM9b25saW5lIiwicm9sZXMiOlsiQzNfVEVBQ0hFUiJdLCJwZXJtaXNzaW9ucyI6WyJSUF9BTkFMWVRJQ1NAUkVBRCIsIlJQX0FQUFNAUkVBRCIsIlJQX0NMQVNTRVMiLCJSUF9DT05URU5UIiwiUlBfREFTSEJPQVJEQFJFQUQiLCJSUF9MQU5ESU5HX1BBR0VAUkVBRCIsIlJQX0xFQVJOSU5HX1BBVEhTIiwiUlBfVVNFUlNAUkVBRCIsIlJQX1dJUkVMRVNTX0FDQ0VTUyJdLCJtZXRhRGF0YSI6eyJwcm9maWxlIjoiQzNUZWFjaGVyIn0sImlhdCI6MTcxNTY5OTA1MiwiZXhwIjoxNzE1Nzg1NDUyfQ.TeHGoAypbgE9IaaSqbjGCe_YkPXriRbMZulPrO5FkkM'

params = {
    "includeNodes": "true",
    "pageSize": 2000,  
}

headers = {
    'Authorization': f'Bearer {bearer_token}',
}

#En caso de que sea verdadero se guardarán los datos en el archivo correspondiente
state=False 

# contiene los elementos filtrados
filtered_data_folder=[]
filtered_data = []
json_data=""

# Define la ruta completa donde quieres guardar el archivo
ruta_destino ="./respuesta.json"
endpoint= f"/backend/v1/contents/62c11f7fc85ef400407f047e"

r=requests.get(url_base + endpoint, headers=headers, params=params)
if r.status_code == 200:
    json_data_carpetas = r.json()
    for data in json_data_carpetas.get("nodes"):
        if (data.get("type")=="DIRECTORY"):
            filtered_item={
                "id": f"{data.get("_id")}",
                "name":(data.get("name").replace(" - ","-")).replace(" ", "-"),
            }
            filtered_data_folder.append(filtered_item)
    json_data_carpetas=filtered_data_folder
    print(json_data_carpetas)
    
    
    for carpeta in json_data_carpetas:
        print(f"=========CARPETA {carpeta.get("name")}========")
        nro=(carpeta.get("id"))
        endpoint= f"/backend/v1/contents/{nro}"
          # Realiza la solicitud GET a la API
        response = requests.get(url_base + endpoint, headers=headers, params= params)

        # Verifica si la solicitud fue exitosa
        if response.status_code == 200:
            # Decodifica la respuesta en formato JSON
            json_data = response.json()
            es_directorio = (json_data.get("type")=="DIRECTORY")
            print( es_directorio )
            if  es_directorio:
                print(f" obteniendo {json_data.get("totalNodes", "No encontrado")} datos")
                for item in json_data.get("nodes"): 
                    
                    filtered_item = {
                        "url": item.get("url"),
                        "Titulo": nombre_sin_extension(item),
                        "thumbnail":item.get("thumbnail","/contents/00-perueduca-secundaria/.thumbnail-3da3d9")
                    }
                    filtered_data.append(filtered_item)
                json_data =filtered_data

                state=True
            # print(json_data)
        else:
            print(f"Error al realizar la solicitud: {response.status_code}")
            state = False
            
    if state:        
        # Guarda el JSON en un archivo local en la ruta especificada
        with open(ruta_destino, "w", encoding="utf-8") as archivo_json:
            json.dump(json_data, archivo_json, ensure_ascii=False, indent=4)

        print(f"La respuesta se ha guardado exitosamente en '{ruta_destino}'.")
            
else:
    print(f"Error al realizar la solicitud: {r.status_code}")
    print(r.text)




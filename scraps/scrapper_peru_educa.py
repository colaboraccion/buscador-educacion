import requests
import json

def concatResponse(response): #concatena las respuestas que son listas
    return ', '.join(response)

def concatResponse2(response):#concatena las respuestas que contienen una propiedad description en cada elemento de la lista
    l=[]
    for e in response:
        l.append(e.get("description"))
    return ', '.join(l)

def checkResponse(response): #verifica que la respuesta no esté vacía
    if response in ["--Seleccione--", "No aplica"]:
        return ""
    else:
        return response

def checkItem(item): #verifica que un item exista
    if item is not None:
        if isinstance(item, list) and len(item)==0:
            return False
        else:
            return True
    else:
        return False
    

# Configura la URL base, el endpoint y los encabezados con el token Bearer
url_base = "https://api.perueduca.pe"

bearer_token='eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJuTzkxQXUyTkU4NXFua3h2N3ZLYjNlNUlNVFh1SWF2ZHQ2S1dPVGE0ZGlNIn0.eyJleHAiOjE3MTUxMDA0NDMsImlhdCI6MTcxNTEwMDE0MywianRpIjoiNjAwMjdiMjEtN2YxNy00YjQ0LTkyZTktMTcxYTFlNTVlZjE2IiwiaXNzIjoiaHR0cHM6Ly9pZGVudGlkYWQubWluZWR1LmdvYi5wZS9yZWFsbXMvcGUtd2ViIiwiYXVkIjpbInBlLndlYi1hcHAucG9ydGFsLWxlZ2FjeSIsInBlLndlYi1zZXJ2aWNlcy5jb21tZW50cyIsInBlLndlYi1zZXJ2aWNlcy51c2VyLmFjY291bnRzIiwicGUud2ViLXNlcnZpY2VzLnNlYXJjaCIsInBlLndlYi1hcHAucG9ydGFsIiwicGUud2ViLXNlcnZpY2VzLmVkdWNhdGlvbmFsLW1hdGVyaWFscyIsInBlLndlYi1zZXJ2aWNlcy5tZXRhZGF0YSIsInBlLndlYi1zZXJ2aWNlcy5yYXRpbmdzIiwicGUud2ViLXNlcnZpY2VzLnN0cmVhbWluZ3MiLCJwZS53ZWItc2VydmljZXMuYW5hbHl0aWNzIiwicGUud2ViLXNlcnZpY2VzLndlYi1jb250ZW50cyJdLCJzdWIiOiI0YTAxZDlhMS00YmE2LTQ3MGYtYTkzMS1hNmEyZDU3MGUyMjEiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJwZS5zZXJ2aWNlLWFjY291bnQub24tZ3Vlc3QiLCJyZXNvdXJjZV9hY2Nlc3MiOnsicGUud2ViLWFwcC5wb3J0YWwtbGVnYWN5Ijp7InJvbGVzIjpbInByb2ZpbGU6Z3Vlc3QiXX0sInBlLndlYi1zZXJ2aWNlcy5jb21tZW50cyI6eyJyb2xlcyI6WyJjb21tZW50czp2YWx1ZXM6cmVhZCJdfSwicGUud2ViLXNlcnZpY2VzLnVzZXIuYWNjb3VudHMiOnsicm9sZXMiOlsidXNlci1hY2NvdW50czpzb2NpYWwtbWVkaWE6cmVhZCJdfSwicGUud2ViLXNlcnZpY2VzLnNlYXJjaCI6eyJyb2xlcyI6WyJzZWFyY2g6aW5kZXg6cmVhZCJdfSwicGUud2ViLWFwcC5wb3J0YWwiOnsicm9sZXMiOlsicHJvZmlsZTpndWVzdCJdfSwicGUud2ViLXNlcnZpY2VzLmVkdWNhdGlvbmFsLW1hdGVyaWFscyI6eyJyb2xlcyI6WyJlZHVjYXRpb25hbC1tYXRlcmlhbHM6dmFsdWVzOnJlYWQiXX0sInBlLndlYi1zZXJ2aWNlcy5tZXRhZGF0YSI6eyJyb2xlcyI6WyJtZXRhZGF0YTpuYW1lOnJlYWQiXX0sInBlLndlYi1zZXJ2aWNlcy5yYXRpbmdzIjp7InJvbGVzIjpbInJhdGluZ3M6dmFsdWVzOnJlYWQiXX0sInBlLndlYi1zZXJ2aWNlcy5zdHJlYW1pbmdzIjp7InJvbGVzIjpbInN0cmVhbWluZ3M6cmVhZCJdfSwicGUud2ViLXNlcnZpY2VzLmFuYWx5dGljcyI6eyJyb2xlcyI6WyJhbmFseXRpY3M6dmlld3M6cmVhZCIsImFuYWx5dGljczp2aWV3czp3cml0ZSJdfSwicGUud2ViLXNlcnZpY2VzLndlYi1jb250ZW50cyI6eyJyb2xlcyI6WyJ3ZWItY29udGVudHM6Y2F0ZWdvcmllczpyZWFkIiwid2ViLWNvbnRlbnRzOnJlc291cmNlczpyZWFkIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJjbGllbnRIb3N0IjoiMTAuMjU1LjI1NS40MiIsImNsaWVudElkIjoicGUuc2VydmljZS1hY2NvdW50Lm9uLWd1ZXN0IiwicHJlZmVycmVkX3VzZXJuYW1lIjoic2VydmljZS1hY2NvdW50LXBlLnNlcnZpY2UtYWNjb3VudC5vbi1ndWVzdCIsImNsaWVudEFkZHJlc3MiOiIxMC4yNTUuMjU1LjQyIn0.SOixiVVNtp8zxFfyB2Mhvczl3dOVDcCsOd9QQxggMYZ4qoHvHKRe0P9Kvmmkba_MG5UjWyUDNozXhhq_BO9ly8Sgm-Oj6GQ5yAj-4xTYLEyWtCX0t-iIBWo_vzvJ0j5Ekcm2-fy3gNU63ukEzQwCJzkb-58qUc17BKOnomITHhE8LHQ0UnDYEJ7wZevWbfNrLI5zxiq01vi1bDqAPbq38qdx0Ct0ZXXQ5kTsZ01z2ZbKJYlS_5VTy_sVO3lT_KQWrLkFgmLkaLnvdnxL8e2FF8lNC4PujMa5W-Ce54n3Ecoel9jHmw1rR1_0v81fXnMzqLcztvJWUGZzGVp5d5DObQ'

headers = {
    'Authorization': f'Bearer {bearer_token}'
}

#En caso de que sea verdadero se guardarán los datos en el archivo correspondiente
state=False 

# contiene los elementos filtrados
filtered_data_folder=[]
filtered_data = []
json_data=""

# Define la ruta completa donde quieres guardar el archivo
ruta_destino ="./respuesta.json"
endpoint= f"/pe-educational-materials/v1/metadata/collections-category/f1a1dd88-cfe6-4a01-ac22-984780aea824/collections"

r=requests.get(url_base + endpoint, headers=headers)

if r.status_code == 200:
    json_data_carpetas = r.json()
    for data in json_data_carpetas:
        filtered_item={
            "id":data.get("id"),
            "name":(data.get("slug").replace(" - ","-")).replace(" ", "-"),
            "size":data.get("publishedResourcesCounter"),
        }
        filtered_data_folder.append(filtered_item)
    json_data_carpetas=filtered_data_folder
    
    for carpeta in json_data_carpetas:
        print(f"=========CARPETA {carpeta.get("name")}========")
        endpoint= f"/pe-educational-materials/v1/resources/educational-materials:values/collections/{carpeta.get('id')}"
        
        paginas=1
        maxPaginas=1
        if(carpeta.get('size')>100):
            maxPaginas=round(carpeta.get('size')/100)
        for i in range(1,maxPaginas+1):
            
            query = f"?page={i}&limit=100"

            # Realiza la solicitud GET a la API
            response = requests.get(url_base + endpoint+query, headers=headers)

            # Verifica si la solicitud fue exitosa
            if response.status_code == 200:
                # Decodifica la respuesta en formato JSON
                json_data = response.json()
                print(f" obteniendo entre {(i-1)*100} y {len(json_data)+(i-1)*100} datos")

                for item, count in  zip(json_data, range((i-1)*100,len(json_data)+(i-1)*100)):
                    
                    filtered_item = {
                        "id": f"educa_secundaria_{carpeta.get("name")}_{count}",
                        "url": f"https://www.perueduca.pe/#/home/materiales-educativos/detalle/{item.get("niceUrl")}",
                        "Titulo": item.get("name"),
                        "Autor": item.get("authors")[0].get("description") if checkItem(item.get("authors")) else ("" and print(f"El item {item.get("Titulo")} no posee la propiedad authors")),
                        "Resumen": item.get("summary").replace(",", ""),
                        "Tema": concatResponse(item.get("keywords")) if checkItem(item.get("keywords")) else "",
                        "Año": checkResponse( item.get("year") if checkItem(item.get("year")) else "No aplica"),
                        "Formato": item.get("format").get("description") if checkItem(item.get("format")) else "",
                        "Fuente": concatResponse2(item.get("source")),
                        "Idioma/Lengua": concatResponse2(item.get("language")) if checkItem(item.get("language")) else "",
                        "Modalidad": item.get("modalities")[0].get("modality").get("description") if checkItem(item.get("modalities")) else ("" and print(f"El item {item.get("Titulo")} no posee la propiedad modalities")),
                        "Nivel educativo/Ciclo": concatResponse2(item.get("modalities")[0].get("level"))  if checkItem(item.get("modalities")) else "",
                        "Grado/Edad": concatResponse2(item.get("modalities")[0].get("degree")) if checkItem(item.get("modalities")) else "",
                        "Área Curricular": concatResponse2(item.get("modalities")[0].get("area")) if checkItem(item.get("modalities")) else "",
                        "Estrategia": checkResponse(item.get("strategy").get("description") if checkItem(item.get("strategy")) else "No aplica"),
                        "Competencia": checkResponse( concatResponse2(item.get("skills")) if checkItem(item.get("skills")) else "No aplica"),
                        "Enfoques": checkResponse(concatResponse2(item.get("transversalApproach"))),
                    }
                    filtered_data.append(filtered_item)
                json_data =filtered_data
                state=True
            else:
                print(f"Error al realizar la solicitud: {response.status_code}")
                print(response.text)
                state = False
                break
    if state:        
        # Guarda el JSON en un archivo local en la ruta especificada
        with open(ruta_destino, "w", encoding="utf-8") as archivo_json:
            json.dump(json_data, archivo_json, ensure_ascii=False, indent=4)

        print(f"La respuesta se ha guardado exitosamente en '{ruta_destino}'.")
            
else:
    print(f"Error al realizar la solicitud: {r.status_code}")
    print(r.text)



